function addNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 + num2;
	return false;
}

document.getElementById('add').addEventListener('click', addNumbers);

function subNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 - num2;
	return false;
}

document.getElementById('sub').addEventListener('click', subNumbers);

function multNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 * num2;
	return false;
}

document.getElementById('mult').addEventListener('click', multNumbers);

function divNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 / num2;
	return false;
}

document.getElementById('div').addEventListener('click', divNumbers);

function moduNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 % num2;
	return false;
}

document.getElementById('modu').addEventListener('click', moduNumbers);

function clearNumbers() {
	document.getElementById('num1').value = " ";
	document.getElementById('num2').value = " ";

	document.getElementById('result').innerHTML = 'Operation will appear here.';
	return false;
}

document.getElementById('clear').addEventListener('click', clearNumbers);